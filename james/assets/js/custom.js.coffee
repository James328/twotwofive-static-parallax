makeRatingStars = (apartment_id, apartment_rating) ->
  $ ->
    $(".rating-for-#{apartment_id}").raty
      path: '/assets/'
      readOnly: true
      score: apartment_rating